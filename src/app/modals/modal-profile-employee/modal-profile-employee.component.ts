import { Component, OnInit, setTestabilityGetter, Input } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { IGetDataEmployee } from "src/app/interfaces/public";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { DatetimeServiceService } from "../../services/datetime-service.service";
import { HttpApiServiceService } from "../../services/http-api-service.service";
import { BrowserStack } from "protractor/built/driverProviders";
import { SignalrServiceService } from "src/app/services/signalr-service.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-modal-profile-employee",
  templateUrl: "./modal-profile-employee.component.html",
  styleUrls: ["./modal-profile-employee.component.css"]
})
export class ModalProfileEmployeeComponent implements OnInit {
  @Input() public getData;
  @Input() public modeForm;
  bSubscription: any;

  profile: IGetDataEmployee;
  dateNow: string = "";
  textModeForm: string = "";
  constructor(
    private modalService: NgbModal,
    private dateTime: DatetimeServiceService,
    private signalR: SignalrServiceService
  ) {
    setTimeout(() => {
      this.bSubscription = this.signalR.hideModalMethodCalled$.subscribe(
        data => {
          this.hideModalProfile(data).subscribe(data => {
            console.log(data);
          });
        }
      );
    }, 300);
  }

  ionViewWillEnter() {}
  ngOnInit() {
    this.dateTime.getFormatDate(new Date()).subscribe(date => {
      this.dateNow = date;
    });

    this.profile = this.getData[0];
    this.modeForm = this.modeForm;
    console.log(this.profile);
    switch (this.modeForm) {
      case 1:
        this.modeAsistance();
        break;

      case 2:
        this.modePay();
        break;

      case 3:
        this.modeProfile();
        break;

      default:
        break;
    }

    // console.log(this.modeForm);
    // let sd =document.getElementById("myNav").style.height = "100%";
    // console.log(sd);

    setTimeout(() => {
      console.log(this.profile.name);
      // this.modalService.dismissAll();
    }, 500);
  }

  modeAsistance() {
    console.log("Modo Assistance..");
    this.dateTime
      .getFormatDateTime(new Date().toISOString())
      .subscribe(datetime => {
        this.textModeForm = "Agregando Asistencia.  =>     " + " " + datetime;
      });

    setTimeout(() => {
      this.modalService.dismissAll();
    }, 4000);
  }

  modePay() {
    this.dateTime
      .getFormatDateTime(new Date().toISOString())
      .subscribe(datetime => {
        this.textModeForm = "   Espere su Pago...      "; //+ " " + datetime;
      });
    // setTimeout(() => {
    //   this.modalService.dismissAll();
    // }, 30000);
  }

  modeProfile() {
    this.dateTime
      .getFormatDateTime(new Date().toISOString())
      .subscribe(datetime => {
        this.textModeForm = " Verificando Perfil    "; //+ " " + datetime;
      });

    setTimeout(() => {
      this.modalService.dismissAll();
    }, 30000);
  }

  hideModalProfile(data): Observable<any> {
    const result = new Observable<any>(observe => {
      this.modalService.dismissAll();
      observe.next(data);
    });

    return result;

    //this.modalIsShow = true;
    // setTimeout(() => {
    //   this.modalIsShow = false;
    // }, 5000);
    // console.log(value);
  }
}
