import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModalProfileEmployeeComponent } from './modals/modal-profile-employee/modal-profile-employee.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule } from '@angular/common/http';
import { CapitalizePipe } from './pipes/capitalize.pipe';
@NgModule({
  declarations: [
    AppComponent,
    ModalProfileEmployeeComponent,
    CapitalizePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule 
  ],
  entryComponents:[ModalProfileEmployeeComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
