import { Injectable } from '@angular/core';

import momentTZ from 'moment-timezone';
import { Observable } from 'rxjs';
import { delay } from "rxjs/operators";

const tz = "America/Santo_Domingo";
@Injectable({
  providedIn: 'root'
})
export class DatetimeServiceService {

  constructor() {
    var sd2 = momentTZ.tz.setDefault(tz);
   }

   getDate(): Observable<Date> {
    // moment()..setDefault("America/Santo_Domingo");
    const result = new Observable<Date>(observable => {
      var date = momentTZ(new Date().toISOString()).format("MM/DD/YYYY");
      observable.next(date);
      //'
    });

    return result;
  }

  convertStringToDate(date: string) {
    // moment()..setDefault("America/Santo_Domingo");

    var date = momentTZ(date).format("MM/DD/YYYY");


    return date;
  }

  
  getFormatDate(date: Date): Observable<any> {
    // moment()..setDefault("America/New_York");
    const result = new Observable(observable => {
      var dayWeek = this.transFormDayString(momentTZ(date).format("d"));
      var day = momentTZ(date).format("DD");
      var month = momentTZ(date).format("MM");
      var monthString = this.transFormMonthString(momentTZ(date).format("MM"));
      var year = momentTZ(date).format("YYYY");

      observable.next(dayWeek + " " + day + ", " + monthString + " " + year);
    });

    return result;
  }

  getFormatDateTime(datetime: string): Observable<any> {
    // moment().tsetDefault("America/New_York");
    const result = new Observable(observable => {
      // var dayWeek = this.transFormDayString(moment(date).tz(this.timeZone).format('d'));
      // var day = moment(date).tz(this.timeZone).format('DD');
      // var month = moment(date).tz(this.timeZone).format('MM');
      // var monthString = this.transFormMonthString(moment(date).tz(this.timeZone).format('MM'));
      // var year = moment(date).tz(this.timeZone).format('YYYY');

      var send = momentTZ(new Date(datetime)).format("hh:mm:ss A");

      observable.next(send);
    });

    return result;
  }

  transFormDayString(day: string) {
    switch (day) {
      case "0":
        return "Dom";
        break;
      case "1":
        return "Lun";

        break;
      case "2":
        return "Mar";

      case "3":
        return "Mie";

      case "4":
        return "Jue";

      case "5":
        return "Vie";

      case "6":
        return "Sab";

      default:
        return "null";
        break;
    }
  }

  transFormMonthString(month: string) {
    switch (month) {
      case "01":
        return "Ene";

      case "02":
        return "Feb";

      case "03":
        return "Mar";
      case "04":
        return "Abr";

      case "05":
        return "May";

      case "05":
        return "Jun";

      case "07":
        return "Jul";
      case "08":
        return "Ago";
      case "09":
        return "Sep";
      case "10":
        return "Oct";
      case "11":
        return "Nov";
      case "12":
        return "Dic";

      default:
        return "null";
    }
  }

  getUniqueId(): Observable<number> {
    // moment.tz.setDefault("America/Santo_Domingo");
    const result = new Observable<number>(observe => {
      setTimeout(async () => {
        var dateSend = await this.returnFecha();//moment( await this.returnFecha()).format("YYDDMMHmmss");
        observe.next(dateSend);
      }, 100);
    });
    return result;
  }

 async  getUniqueIdNumber() {
   
    var id = await this.returnFecha();//moment( await this.returnFecha()).format("YYDDMMHmmss");
    return id;

  }

  async returnFecha() {
    // console.log(new Date(Date.now()))
    await delay(0);
    return await Date.now()

  }
}
