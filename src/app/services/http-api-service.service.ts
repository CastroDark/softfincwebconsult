import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ISendDataRequestBasic } from "../interfaces/public";
import { Observable } from "rxjs";
import * as configuration from "../configuration.json";
@Injectable({
  providedIn: "root"
})
export class HttpApiServiceService {
  constructor(private http: HttpClient) {}

  url: string = configuration.connectionApiClient; //"https://172.16.18.121:44"//data + this.fingeHub;

  //url = 'http://10.10.0.101:20814';
  GetProfileEmployeePunch(
    idEployee: string,
    idCompany: string
  ): Observable<any> {
    let data: ISendDataRequestBasic = {
      date1: "2019-01-01",
      date2: "2019-01-01",
      query1: idEployee,
      query2: idCompany
    };
    return this.http.post(
      `${this.url}/api/PunchFinger/GetProfilePunchEmployee`,
      data
    );
  }
}
