import { Component, Inject, OnInit, ViewChild } from "@angular/core";
import { ModalProfileEmployeeComponent } from "./modals/modal-profile-employee/modal-profile-employee.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SignalrServiceService } from "./services/signalr-service.service";
import { Observable } from "rxjs";
import { DOCUMENT } from "@angular/common";
import { HttpApiServiceService } from "./services/http-api-service.service";
import { IGetDataPunch, IGetDataEmployee } from "./interfaces/public";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  private timer;
  bSubscription: any;
  modalIsSHow: boolean = false;
  isfullscreen;
  elem;

  modeForm: number = 0;

  getDataEmployee: IGetDataEmployee;

  title = "Pagina de Consulta.";

  constructor(
    private modalCtrl: NgbModal,
    private signalR: SignalrServiceService,
    @Inject(DOCUMENT) private document: any,
    private http: HttpApiServiceService
  ) {
    this.elem = document.documentElement;

    this.bSubscription = this.signalR.showModalProfileMethodCalled$.subscribe(
      data => {
        this.showModalProfile(data).subscribe(data => {
          // console.log(data);
        });
      }
    );
  }

  ngOnInit(): void {
    // JavaScript

    setTimeout(() => {
      this.openFullscreen();
    }, 7000);

    setTimeout(() => {
      // this.conecctSignal();
      this.signalR.startConnection().then(data => {
        this.signalR.addSuscribeToNotifiPushOnline();
        this.signalR.addSuscribeToCloseModalPushOnline();
        // this.signalR.callAddUser();
      });
    }, 3000);

    this.timer = setInterval(_ => {
      this.signalR.statusConecction().then(
        data => {
          if (!data) {
            console.log("desconectado");
            window.location.reload();
          } else {
            console.log("conectado");
          }
        },
        error => {
          console.log("Error");
          window.location.reload();
        }
      );
    }, 10000);
  }

  conecctSignal() {
    const docWithBrowsersExitFunctions = document as Document & {
      mozCancelFullScreen(): Promise<void>;
      webkitExitFullscreen(): Promise<void>;
      msExitFullscreen(): Promise<void>;
    };
  }

  openFullscreen() {
    this.openfullscreen();
  }
  openfullscreen() {
    // Trigger fullscreen
    const docElmWithBrowsersFullScreenFunctions = document.documentElement as HTMLElement & {
      mozRequestFullScreen(): Promise<void>;
      webkitRequestFullscreen(): Promise<void>;
      msRequestFullscreen(): Promise<void>;
    };

    if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
      docElmWithBrowsersFullScreenFunctions.requestFullscreen();
    } else if (docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen) {
      /* Firefox */
      docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen();
    } else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen();
    } else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) {
      /* IE/Edge */
      docElmWithBrowsersFullScreenFunctions.msRequestFullscreen();
    }
    this.isfullscreen = true;
  }

  /* Close fullscreen */
  closeFullscreen() {
    const docWithBrowsersExitFunctions = document as Document & {
      mozCancelFullScreen(): Promise<void>;
      webkitExitFullscreen(): Promise<void>;
      msExitFullscreen(): Promise<void>;
    };
    if (docWithBrowsersExitFunctions.exitFullscreen) {
      docWithBrowsersExitFunctions.exitFullscreen();
    } else if (docWithBrowsersExitFunctions.mozCancelFullScreen) {
      /* Firefox */
      docWithBrowsersExitFunctions.mozCancelFullScreen();
    } else if (docWithBrowsersExitFunctions.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      docWithBrowsersExitFunctions.webkitExitFullscreen();
    } else if (docWithBrowsersExitFunctions.msExitFullscreen) {
      /* IE/Edge */
      docWithBrowsersExitFunctions.msExitFullscreen();
    }
    this.isfullscreen = false;
  }

  showModal() {
    const modal = this.modalCtrl.open(ModalProfileEmployeeComponent, {
      windowClass: "xlModal"
    });
    modal.componentInstance.getData = this.getDataEmployee;
    modal.componentInstance.modeForm = this.modeForm;
    modal.result.then(
      this.handledCloseModal.bind(this),
      this.handledCloseModal.bind(this)
    );
  }

  showModalProfile(data): Observable<any> {
    const result = new Observable<any>(observe => {
      if (!this.modalIsSHow) {
        // this.showModal();

        let dataPunch: IGetDataPunch = data;
        this.modeForm = dataPunch.idActionPush;
        this.http
          .GetProfileEmployeePunch(
            String(dataPunch.idEmployee),
            dataPunch.idCompany
          )
          .subscribe(
            data => {
              console.log(data);
              this.getDataEmployee = data.data;
              if (this.getDataEmployee) {
                this.showModal();
              }
            },
            error => {
              console.log(error);
            }
          );

        this.modalIsSHow = true;
      }

      observe.next(data);
    });

    return result;

    //this.modalIsShow = true;
    // setTimeout(() => {
    //   this.modalIsShow = false;
    // }, 5000);
    // console.log(value);
  }

  handledCloseModal() {
    this.modalIsSHow = false;
    console.log("cERRO mODAL");
  }
}
